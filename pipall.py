#!/usr/bin/env python3

import sys
import subprocess as sp
import os
import pathlib
import time

#================= import packages hard way=====================
import_list0=['fire', 'alive_progress']
import_list=['importlib_resources','pandas','xvfbwrapper','matplotlib','scipy']


#==========================================install check_call======
def install(package):
    sp.check_call([sys.executable, "-m", "pip", "install", package])

#==========================================install import======
def install_and_import(packagelist):
    import importlib
    importnames={}
    for i in packagelist:
        importnames[i]=i
    # BUT some packages have different names
    importnames['opencv-python']="cv2"

    for i in packagelist:
        try:
            importlib.import_module( importnames[i] )
        except ImportError:
            install(i)
        finally:
            print("I... importing",i) # check if fire is imported
            globals()[i] = importlib.import_module( importnames[i] )


#
#====================================== imports after install
#
print("================ import fire and alive_progress=============")
install_and_import( import_list0 )  # fire and alive
# doensnt import!!!!!
from fire import Fire
from alive_progress import alive_bar,config_handler


#======================================================NORMAL CODE
#
#======================================================NORMAL CODE




#====================================================== RUN CODE
def launch(CMD, fake=False):
    print("D... launchng", CMD)
    if not fake:
        res=sp.check_output( CMD.split() ).decode("utf8").rstrip()
        #print("D...",res)
        return res




#======================================================main
def main():


    print("================ import the rest ===========================")
    print( import_list )
    ll=len(import_list)
    config_handler.set_global( length=26) # 25 was OK
#    config_handler.set_global( length=30) # 25 was OK
    with alive_bar( ll ) as bar:   # declare your expected total
        for i in range( ll ):
#            if i%10==0:
            print("D... i",i, import_list[i] )
            bar.text( import_list[i] )
            install_and_import( [import_list[i]] )
            time.sleep(0.2)





#========================================================MAIN======
#========================================================MAIN======
#========================================================MAIN======
if __name__=="__main__":
    Fire( main )
