#+OPTIONS: toc:nil        (no default TOC at all)
#+OPTIONS: num:nil
#+OPTIONS: creator:nil
#+OPTIONS: author:nil
# +OPTIONS: date:nil
#+OPTIONS: tex:t
#+LATEX_CLASS_OPTIONS: [a4paper,10pt]
#+LATEX_HEADER: \usepackage{parskip} \usepackage[margin=1cm]{geometry}  \usepackage[utf8]{luainputenc}
#+OPTIONS: tex:dvipng

#+begin_example
This is verbatim text
#+end_example

* Link and tables ACTAR


https://indico.in2p3.fr/event/18026/contributions/68569/attachments/51952/66963/2019-01_ACTAR-TPC_J-Giovinazzo_ENSAR-GDSpub.pdf

Original - presentation -  table2

|       | mbar | protons/cm2 |              |                |
| C4H10 |   76 |     9.25e19 | 71% protons  |                |
| H2    | 1000 |     2.47e20 | 100% protons | dE/dx as above |


Original - presentation -  table2

| mix         |                | # prot/cm2@2mm | % proton purity |       | my results |
|-------------+----------------+----------------+-----------------+-------+------------|
| H2          |           1000 |        9.88e18 |             100 | 1     |    9.88e+18 |
| H2+C4H10 2% |            813 |        8.58e18 |              96 |       |  8.677e+18 |
| H2+C4H10 5% |            626 |        7.29e18 |              92 |       |   7.42e+18 |
| C4H10       |             76 |         3.7e18 |              71 | 10/14 |  3.755e+18 |
| eq CH2      | dE/dx 40ug/cm2 |        3.42e18 |              66 | 2/3   |   3.42e+18 |


| # protons | rel.p content |
|-----------+---------------|
|   9.88e18 |            1. |
|   8.58e18 |    0.86842105 |
|   7.29e18 |    0.73785425 |
|    3.7e18 |    0.37449393 |
|   3.42e18 |    0.34615385 |
 #+TBLFM: @2$2=$1/@2$1::@3$2=@3$1/@2$1::@4$2=@4$1/@2$1::@5$2=@5$1/@2$1::@6$2=@6$1/@2$1


**  Considerations

 - gas density goes with pressure p/T ... (m/M)RT/V=rho RT/M=p
   - pV=nRT ... pV=mRT/M ... pM/RT=m/V=\rho
 - Na = 6.022e+23 /mol
 - R =8.314462618 J/mol/K
 - area density relates to atom density $ m/S = N/S * M/Na $
 - carefully with single atom vs. two-atom molecules

*** Molar masses

| element |      M |
|---------+--------|
| H       |  1.008 |
| H2      |  2.016 |
| C       | 12.011 |
| Cl      | 35.453 |
| C4H10   | 58.124 |
#+TBLFM: @6$2=12.011*4+10*1.008


*** Calculate different scenarios /presentation/

*Presentation evidently uses NTP ... temperature 20C and 100.000 kPa*

 #+NAME: vartable_h2
| Variable |       Value |         |        H2  pure     |
|----------+-------------+---------+---------------------|
| R        | 8.314462618 | J/K/mol |                     |
| Na       |   6.022e+23 | 1/mol   |                     |
| p        |      100000 | Pa      |  1000mb          |
| T        |      293.15 | K       | 20C                 |
| M        |       2.008 | g/mol   |                     |
| t        |         0.2 | cm      |                     |
| apm      |           2 |         | 2 atoms in molecule |
| rho      | 8.23833e-05 | g/cm3   | RESULT              |
| N/S @2mm |  9.8827e+18 |         | RESULT    9.88e18   |


 #+NAME: vartable_mix2
| Variable    |       Value |         | 2%                |
|-------------+-------------+---------+-------------------|
| R           | 8.314462618 | J/K/mol |                   |
| Na          |   6.022e+23 | 1/mol   |                   |
| p           |       81300 | Pa      | --mb              |
| T           |      293.15 | K       | 20C               |
| M           |     3.13816 | g/mol   | 2% C4H10          |
| t           |         0.2 | cm      |                   |
| apm         |        2.16 |         | atoms in molecule |
| rho         |  0.00010133 | g/cm3   | RESULT            |
| N/S@2mm     |           0 |         | RESULT            |
| protons@2mm |   8.677e+18 |         | RESULT  8.58e18   |
 #+TBLFM: @6$2=0.98*2.016+0.02*58.124::@8$2=0.98*2+0.02*10


 #+NAME: vartable_c4h10
| Variable    |     Value |         |       C4H10       |
|-------------+-----------+---------+-------------------|
| R           | 8.3144626 | J/K/mol |                   |
| Na          | 6.022e+23 | 1/mol   |                   |
| p           |      7600 | Pa      | 76mb              |
| T           |    293.15 | K       | 20C               |
| M           |    58.124 | g/mol   |                   |
| t           |       0.2 | cm      |                   |
| apm         |        10 |         | atoms in molecule |
| rho         | 0.0010333 | g/cm3   | RESULT            |
| N/S@2mm     |         0 |         | RESULT            |
| protons@2mm | 3.755e+18 |         | RESULT  3.7e18    |
 #+TBLFM: @6$2=4*12.011+10*1.008

 #+NAME: vartable_mix5
| Variable    |     Value |         |      5%           |
|-------------+-----------+---------+-------------------|
| R           | 8.3144626 | J/K/mol |                   |
| Na          | 6.022e+23 | 1/mol   |                   |
| p           |     62600 | Pa      | 626mb             |
| T           |    293.15 | K       | 20C               |
| M           |    4.8214 | g/mol   | 5%c4h10           |
| t           |       0.2 | cm      |                   |
| apm         |       2.4 |         | atoms in molecule |
| rho         | 0.0012383 | g/cm3   | RESULT            |
| N/S@2mm     |         0 |         | RESULT            |
| protons@2mm |  7.42e+18 |         | RESULT  7.29e18   |
 #+TBLFM: @6$2=(4*12.011+10*1.008)*0.05+0.95*2.016::@8$2=10*0.05+2*0.95


 #+NAME: vartable_ch4
| Variable    |     Value |         |     CH4           |
|-------------+-----------+---------+-------------------|
| R           | 8.3144626 | J/K/mol |                   |
| Na          | 6.022e+23 | 1/mol   |                   |
| p           |     17300 | Pa      | ??? mb            |
| T           |    293.15 | K       | 20C               |
| M           |    16.043 | g/mol   |                   |
| t           |       0.2 | cm      |                   |
| apm         |         4 |         | atoms in molecule |
| rho         | 0.0010333 | g/cm3   | RESULT            |
| N/S@2mm     |         0 |         | RESULT            |
| protons@2mm |  3.42e+18 |         | RESULT  3.42e18   |
 #+TBLFM: @6$2=1*12.011+4*1.008::@8$2=4


*For the air - I need to use O2 N2* I must believe that TRIM.IN generated by SRIM is ok. But good rho needs 28.9g/mol
 - at 20C I get *1.2014 mg/cm3 @NTP* and this I use in TRIM.IN
 - at 0C ... *1.28936 mg/cm3 @STP*
 - @NTP engeneeringtoolbox says rho = 1.204
 - I am close with using NTP

 #+NAME: vartable_air
| Variable    |       Value |         | AIR  1.293 @0C            |
|-------------+-------------+---------+---------------------------|
| R           |   8.3144626 | J/K/mol |                           |
| Na          | 6.02214e+23 | 1/mol   |                           |
| p           |      101325 | Pa      | mb                        |
| T           |      293.15 | K       | 20C                       |
| M           |   28.906871 | g/mol   |                           |
| t           |         0.2 | cm      |                           |
| apm         |           1 |         | atoms in molecule         |
| rho         |   0.0012017 | g/cm3   | RESULT 0.00120484 TRIN.IN |
| N/S@2mm     |           0 |         | RESULT                    |
| protons@2mm |           0 |         | RESULT                    |
 #+TBLFM: @6$2=0.00015*12.011+0.210756*15.999*2+0.78442*14.007*2+0.00467*39.948::@8$2=1



#+BEGIN_SRC python :var vars=vartable_air :results output replace
var_dict = {row[0]: float(row[1]) for row in vars}
#print(var_dict)
R = var_dict["R"]
Na = var_dict["Na"]
M = var_dict["M"]
p = var_dict["p"]
T = var_dict["T"]
rho = ( (M/1000) / (R * T)) * p
rho=rho/1000 # from kg/m3 -> g/cm3
print(rho,"g/cm3")
#
t = var_dict["t"]
apm = var_dict["apm"]
atomss_in_t=t*rho*Na/M # N/S
print(atomss_in_t,"  atoms in ",t,"cm")
print(atomss_in_t*apm,"H atoms in ",t,"cm")
#+END_SRC

#+RESULTS:
: 0.001201693229069688 g/cm3
: 5.006951366344515e+18   atoms in  0.2 cm
: 5.006951366344515e+18 H atoms in  0.2 cm

#+begin_src python :results output replace
Ra=10/14  #0.71
Ra=0.71*0.05+0.95 # proton purity is smaller (92) than 95%
Ra=0.71*0.02+0.98 # proton purity is smaller (92) than 95%
# so it must be by mas
#Ra=0.71 # but 71% is for pure c4h10
#Ra=10/14
print(Ra)
#+end_src

#+RESULTS:
: 0.9942


* Email

** Interrpretation

We stick to the density, believing.

** Text

#+begin_example
Can you please produce and send me a SRIM table for protons
in the actar gas: H2(90%)+iC4H10(10%) :
pressure 962.2 mbar, P(inside) 956 mbar?
Here the settings I used to define the gas. The protons' energies
can go from 0 to 50 MeV. Thank you very much!


Disk File Name = SRIM Outputs\Hydrogen in  H- C- H (gas).txt

 Ion = Hydrogen [1] , Mass = 1,008 amu

 Target Density =  2,8170E-03 g/cm3 = 7,1176E+20 atoms/cm3
 Target is a GAS
 ======= Target  Composition ========
    Atom   Atom   Atomic    Mass
    Name   Numb   Percent   Percent
    ----   ----   -------   -------
      H      1    031,25    013,22
      C      6    012,50    063,00
      H      1    056,25    023,79
 ====================================

kindly
Ana
#+end_example

** Verification of the email

 #+NAME: vartable_mix10
| Variable    |     Value |         |                   |
|-------------+-----------+---------+-------------------|
| R           | 8.3144626 | J/K/mol |                   |
| Na          | 6.022e+23 | 1/mol   |                   |
| p           |    9004000 | Pa      | 956 mb            |
| T           |    293.15 | K       | 20C               |
| M           |    7.6268 | g/mol   |                   |
| t           |       0.2 | cm      |                   |
| apm         |       2.8 |         | atoms in molecule |
| rho         | 0.0003169 | g/cm3   | RESULT  0.002817  |
| N/S@2mm     |         0 |         | RESULT            |
| protons@2mm |         0 |         | RESULT            |
 #+TBLFM: @6$2=(4*12.011+10*1.008)*0.1+0.9*2*1.008::@8$2=0.9*2+0.1*10

#+BEGIN_SRC python :var vars=vartable_mix10 :results output replace
var_dict = {row[0]: float(row[1]) for row in vars}
#print(var_dict)
R = var_dict["R"]
Na = var_dict["Na"]
M = var_dict["M"]
p = var_dict["p"]
T = var_dict["T"]
rho = ( (M/1000) / (R * T)) * p
rho=rho/1000 # from kg/m3 -> g/cm3
print(rho,"g/cm3")
#
t = var_dict["t"]
apm = var_dict["apm"]
atomss_in_t=t*rho*Na/M # N/S
print(atomss_in_t,"  atoms in ",t,"cm")
print(atomss_in_t*apm,"H atoms in ",t,"cm")
#+END_SRC

#+RESULTS:
: 0.02817434067126504 g/cm3
: 4.449202274147954e+20   atoms in  0.2 cm
: 1.2457766367614271e+21 H atoms in  0.2 cm


*** DONE Check it sums to 100

Atomic SUM - original
| 100. | #+TBLFM: @1$1=31.25+12.5+56.25 |

Mass SUM - original
| 100.01 | #+TBLFM: @1$1=13.22 + 63.00 + 23.79 |


*** DONE Stoichiometry == atomic composition

I have 10 parts isobutane and 90 parts of H

H2 + C4H10 ; =9= parts + =1= part ; =18 + 4 + 10= = 32

atoms/total - it is not exactly stochiometry, but also not too far:

|      0.5625 | 56.25 |
|       0.125 |  12.5 |
|      0.3125 | 31.25 |
|          1. |       |
|-------------+-------|
| my division | Email |
 #+TBLFM: @1$1=18/32::@2$1=4/32::@3$1=10/32::@4$1=vsum(@1..@3)

*** DONE Check ratio between H and H
Ratio 10H / 2H  from original
|       1.8 |
| 1.7995461 |
#+TBLFM: @2$1=23.79/13.22::@1$1=56.25/31.25

I take 90% * MMass H2 / 10%*MMass H10 and it makes the correct ratio 1.8
| 1.8 |
|-----|
#+TBLFM: @1$1=0.9*2.016/0.1*10*1.008

*** Total MMass 7.6268  g/mol
Total MMass with ratios should be 7.6268 - BUT WHY I GOT 6.72 before?? My Error
| 7.6268 | #+TBLFM: @1$1=0.9*2.016+0.1*(10*1.008+4*12.011) |


*** Mass composition total

| 76.268 |
#+TBLFM: @1$1=18*1.008+4*12.011+10*1.008

*** Mass composition

 | 0.23789794 |      23.79 |
 | 0.62993654 |      63.00 |
 | 0.13007598 |      13.22 |
 |------------+------------|
 |    my calc | email mass |
 #+TBLFM: @1$1=18*1.008/76.268::@2$1=4*12.011/76.268::@3$1=10/1.008/76.268

* XCheck  STP

https://www.geeksforgeeks.org/stp-vs-ntp/

*At STP 1 mole of any gas occupies 22.4 l*

Density at STP presure and temperature, N2 :
| 1.2498550e-3 |
  #+TBLFM: @1$1=(14.007*2/1000 / (8.3144*273.15)) *101325 /1000

Density at STP presure and temperature, O2 :
| 1.4276026e-3 |
  #+TBLFM: @1$1=(15.999*2/1000 / (8.3144*273.15)) *101325 /1000

Any one mole simply at STP has 22.4 liters:

| 22.413801 | liters |
#+TBLFM: @1$1=(8.3144*273.15/101325)*1000


Any one mole simply at NTP :

| 24.054936 | liters |
#+TBLFM: @1$1=(8.3144*293.15/101325)*1000


https://www.engineeringtoolbox.com/stp-standard-ntp-normal-air-d_772.html#:~:text=NTP%20%2D%20Normal%20Temperature%20and%20Pressure%20%2D%20is%20defined%20as%20air%20at,0.075%20pounds%20per%20cubic%20foot)

#+begin_example
NTP - Normal Temperature and Pressure
NTP is commonly used  as a standard condition  for testing
and documentation of fan capacities:

NTP - Normal Temperature and Pressure - is defined as air
 at 20 oC (293.15 K, 68 oF) and
1 atm ( 101.325 kN/m2, 101.325 kPa, 14.7 psia, 0 psig, 29.92 in Hg, 407 in H2O, 760 torr).

Density 1.204 kg/m3 (0.075 pounds per cubic foot)

At these conditions, the volume of 1 mol of a gas is 24.0548 liters.
#+end_example

* BASIC QUESTION

 1. do I get ACTAR density at 293K and 1000mb or 1013mb ?

 *Answer*:
 - 0.000317 for 101.3kPa 293.15K
 - 0.0003129 1000mb
 - 0.0002816 for 900mb
 - 0.0002785 for 890mb
 - 0.002817 for 9004000 Pa =  90040 mb = 90.04 bar !!!

* Conclusion :

 *Email DENSITY must be a nonsense*
