#!/usr/bin/env python3
#
# Credits for the core table :  mcgarity GitLab
# https://gitlab.com/mcgaritystuff/periodic-table-guesser
# -
# the beauty of his nice python code is now destroyed.
# There was no licence file.

class Element:
  """An element on the periodic table

    This object simply represents an element on the periodic table. It contains
    information related to whether the user has guessed it and if they got it
    correct or not.

    Attributes:
        name: The name of the element (i.e. Hydrogen)
        symbol: The symbol of the element (i.e. "H")
        number: The element's corresponding number
        row: The row on the periodic table the element is on
        col: The column of the periodic table the element is on
        answered: True/False if the user has tried guessing the element yet
        correct: If the user answered correctly or not
  """

  def __init__(self, name: str, symbol: str, number: int, row: int, col: int):
    """ Initializes Element with the given parameters and answered/correct
        to false """
    self._name = name
    self._symbol = symbol
    self._number = number
    self._row = row
    self._col = col

    self._answered = False
    self._correct = False

  def answer_question(self, name: str, symbol: str) -> str:
    """Answers the element with a name and symbol

    Args:
        name: The name of the element
        symbol: The symbol of the element

    Returns:
        A string containing the wrong responses (or blank if nothing is incorrect)

    """

    # Don't allow the user to retry the answer ;)
    if self._answered:
      response = "Answer was already given!"
      return response

    self._answered = True
    self._correct = True
    response = ""

    if name != self._name:
      response += "Name given: "+name+" | Correct name: "+self._name+"\n"
      self._correct = False
    if symbol != self._symbol:
      response += "Symbol given: "+symbol+" | Correct symbol: "+self._symbol+"\n"
      self._correct = False

    return response

  def get_symbol(self) -> str:
    """Get the symbol

    Returns:
        The symbol. If the symbol is 1 character, it returns the symbol + a space

    """

    if len(self._symbol) == 2:
      return self._symbol
    else:
      return self._symbol + ' '

  def get_col(self) -> int:
    """Gets the column

    Returns:
        The column number
    """
    return self._col

  def get_row(self) -> int:
    """Gets the row

    Returns:
        The row number
    """
    return self._row

  def get_answered(self) -> bool:
    """Gets whether the element has been guessed or not

    Returns:
        True or false for if it's been guessed
    """
    return self._answered

  def get_correct(self) -> bool:
    """Get whether the user guessed the element correctly or not

    Returns:
        True for correct, false for incorrect (default, check get_answered first)
    """
    return self._correct


#===============================================================ENDOFCLASS




# 36 x 96 cyans   46-BG
#    WHITE   = '\033[40m' # not really, white on gray

class colors:
    BLUE    = '\033[94m'
    YELLOW  = '\033[93m'
    GREEN   = '\033[92m'
    RED     = '\033[91m'
    GRAY    = '\033[90m'
#    WHITE  = '\033[89m'
    REDBG   = '\033[41m'
    REDDK   = '\033[31m'
    GRAYDK  = '\033[30m'
    WHITE   = '\033[92m' #
    END     = '\033[0m'

periodic_table = """ --                                                 --
|  |                                               |  |
|--|--                               --------------|--|
|  |  |                             |  |  |  |  |  |  |
|--|--|                             |--|--|--|--|--|--|
|  |  |                             |  |  |  |  |  |  |
|--|--|-----------------------------|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
|--|--|  |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 -----|  |--------------------------------------------
       ||_
       |_ --------------------------------------------
         |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
         |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
         |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
          --------------------------------------------
"""

response_message = ""
all_elements = []

def printTable():
  """Prints the current periodic table

  Simply prints the current periodic table based on the elements that have been
  guessed by the user. If the user hasn't guessed the element yet, it's left as
  a blank box. If they have, it'll be green for correct and red for incorrect.

  """

  periodic_table_rows = periodic_table.split('\n')

  # We have to reverse so the colors don't mess up the string splicing
  for element in reversed(all_elements):
    # Skip elements that haven't been answered yet
    if not element.get_answered():
      continue

    # Grab some information so we know where to draw it and the color to draw in
    table_row = element.get_row() * 2 - 1
    table_col = element.get_col() * 3 - 2
    row_text = periodic_table_rows[table_row]
    # color = colors.GREEN if element.get_correct() else colors.RED
    color = colors.WHITE

    # Replace the part of the table with the element symbol
    periodic_table_rows[table_row] = '%s%s%s'\
        %(row_text[:table_col], \
          color + element.get_symbol() + colors.END, \
          row_text[table_col+2:])

  # Print every row to the periodic table now that it's been updated
  for row in periodic_table_rows:
    print(row)

def parse_elements_file():
  """Parses the elements.txt file

  This assumes that the elements in elements.txt are in order by number.
  It reads every line of the file and then parses it into an Element object.
  Each line in the file should follow the format:
    ElementName ElementSymbol ElementNumber PeriodicTableRow PeriodicTableColumn

  """

  #elements_file = open('elements.txt', 'r')
  #contents = []
  #if elements_file.mode == 'r':
  #  contents = elements_file.readlines()
  #
  # print(contents)
  # quit()
  contents = ['Hydrogen      H  1   1   1\n', 'Helium        He 2   1   18\n', 'Lithium       Li 3   2   1\n', 'Beryllium     Be 4   2   2\n', 'Boron         B  5   2   13\n', 'Carbon        C  6   2   14\n', 'Nitrogen      N  7   2   15\n', 'Oxygen        O  8   2   16\n', 'Flourine      F  9   2   17\n', 'Neon          Ne 10  2   18\n', 'Sodium        Na 11  3   1\n', 'Magnesium     Mg 12  3   2\n', 'Aluminum      Al 13  3   13\n', 'Silicon       Si 14  3   14\n', 'Phosphorus    P  15  3   15\n', 'Sulfur        S  16  3   16\n', 'Chlorine      Cl 17  3   17\n', 'Argon         Ar 18  3   18\n', 'Potassium     K  19  4   1\n', 'Calcium       Ca 20  4   2\n', 'Scandium      Sc 21  4   3\n', 'Titanium      Ti 22  4   4\n', 'Vanadium      V  23  4   5\n', 'Chromium      Cr 24  4   6\n', 'Manganese     Mn 25  4   7\n', 'Iron          Fe 26  4   8\n', 'Cobalt        Co 27  4   9\n', 'Nickel        Ni 28  4   10\n', 'Copper        Cu 29  4   11\n', 'Zinc          Zn 30  4   12\n', 'Gallium       Ga 31  4   13\n', 'Germanium     Ge 32  4   14\n', 'Arsenic       As 33  4   15\n', 'Selenium      Se 34  4   16\n', 'Bromine       Br 35  4   17\n', 'Krypton       Kr 36  4   18\n', 'Rubidium      Rb 37  5   1\n', 'Strontium     Sr 38  5   2\n', 'Yttrium       Y  39  5   3\n', 'Zirconium     Zr 40  5   4\n', 'Niobium       Nb 41  5   5\n', 'Molybdenom    Mo 42  5   6\n', 'Technitium    Tc 43  5   7\n', 'Ruthenium     Ru 44  5   8\n', 'Rhodium       Rh 45  5   9\n', 'Palladium     Pd 46  5   10\n', 'Silver        Ag 47  5   11\n', 'Cadmium       Cd 48  5   12\n', 'Indium        In 49  5   13\n', 'Tin           Sn 50  5   14\n', 'Antimony      Sb 51  5   15\n', 'Tellurium     Te 52  5   16\n', 'Iodine        I  53  5   17\n', 'Xenon         Xe 54  5   18\n', 'Cesium        Cs 55  6   1\n', 'Barium        Ba 56  6   2\n', 'Lanthanum     La 57  9   4\n', 'Cerium        Ce 58  9   5\n', 'Praseodymium  Pr 59  9   6\n', 'Neodymium     Nd 60  9   7\n', 'Promethium    Pm 61  9   8\n', 'Samarium      Sm 62  9   9\n', 'Europium      Eu 63  9   10\n', 'Gadolinium    Gd 64  9   11\n', 'Terbium       Tb 65  9   12\n', 'Dysprosium    Dy 66  9   13\n', 'Holmium       Ho 67  9   14\n', 'Erbium        Er 68  9   15\n', 'Thulium       Tm 69  9   16\n', 'Ytterbium     Yb 70  9   17\n', 'Lutetium      Lu 71  9   18\n', 'Hafnium       Hf 72  6   4\n', 'Tantalum      Ta 73  6   5\n', 'Tungsten      W  74  6   6\n', 'Rhenium       Re 75  6   7\n', 'Osmium        Os 76  6   8\n', 'Iridium       Ir 77  6   9\n', 'Platinum      Pt 78  6   10\n', 'Gold          Au 79  6   11\n', 'Mercury       Hg 80  6   12\n', 'Thallium      Tl 81  6   13\n', 'Lead          Pb 82  6   14\n', 'Bismuth       Bi 83  6   15\n', 'Polonium      Po 84  6   16\n', 'Astatine      At 85  6   17\n', 'Radon         Rn 86  6   18\n', 'Francium      Fr 87  7   1\n', 'Radium        Ra 88  7   2\n', 'Actinium      Ac 89  10  4\n', 'Thorium       Th 90  10  5\n', 'Protactinium  Pa 91  10  6\n', 'Uranium       U  92  10  7\n', 'Neptumium     Np 93  10  8\n', 'Plutonium     Pu 94  10  9\n', 'Americium     Am 95  10  10\n', 'Curium        Cm 96  10  11\n', 'Berkelium     Bk 97  10  12\n', 'Californium   Cf 98  10  13\n', 'Einsteinium   Es 99  10  14\n', 'Fermium       Fm 100 10  15\n', 'Mendelevium   Md 101 10  16\n', 'Nobelium      No 102 10  17\n', 'Lawrencium    Lr 103 10  18\n', 'Rutherfordium Rf 104 7   4\n', 'Dubnium       Db 105 7   5\n', 'Seaorgium     Sg 106 7   6\n', 'Bhorium       Bh 107 7   7\n', 'Hassium       Hs 108 7   8\n', 'Meitnerium    Mt 109 7   9\n', 'Darmstadtium  Ds 110 7   10\n', 'Roentgenium   Rg 111 7   11\n', 'Copernicium   Cn 112 7   12\n', 'Nihonium      Nh 113 7   13\n', 'Flerovium     Fl 114 7   14\n', 'Moscovium     Mc 115 7   15\n', 'Livermorium   Lv 116 7   16\n', 'Tennessine    Ts 117 7   17\n', 'Oganesson     Og 118 7   18\n']

  for line in contents:
    line_contents = line.split()
    all_elements.append(
      Element(
        line_contents[0],
        line_contents[1],
        int(line_contents[2]),
        int(line_contents[3]),
        int(line_contents[4])
      )
    )




#============================================ REAL CODE HERE ========

# Parse the elements file first
parse_elements_file()

# Loop through the application as the user guesses elements on the table.
# The loop will be killed once the user has guessed all the elements,
# or types EXIT.
for element in all_elements:
    element_number = element._number
    element_name = element._name
    element_symbol = element._symbol

    #  element_number = 1
    #element_name = 'Hydrogen'
    #element_symbol = 'H'

    if element_number <= 0 or element_number > len(all_elements):
        response_message = colors.RED + "Number must be 1-" + str(len(all_elements)) + colors.END
        continue
    guessing_element = all_elements[element_number - 1]
    response_message = guessing_element.answer_question(element_name, element_symbol)
    if response_message != "":
        response_message = colors.RED + response_message + colors.END
# Clear the screen
print("\033[H\033[J")
printTable()

i = input("Input element:")
for element in all_elements:
   if i.lower() == element._symbol.lower():
     print( "{} Z={}".format(element._name, element._number) )
     for a in range( int(2*element._number-1),int(2.5*element._number),1):
       print("{:3d}{}".format(a,i), end="|")
print()
