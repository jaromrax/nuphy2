# TODO:
  - yields
  - xsections

# Diary of the development:
based on long term experiences of forgetting and rememberring,
I want to summarize the corner stones of this revision:

- it must be runnable from **command line** with one interfacing command: `nuphy`
    - it must be installable via
	    - `pip install -e .`
		- standard `setup.py`
		- later from **PyPI** via `pip3 install nuphy`
	- it must have a development cycle like
	    - `gca`
		- `bumpversion release`
		- `./distcheck`
		- `twine upload --repository-url https://pypi.org/ dist/*`
- the modules must be standalone runnable
- the modules are located in 1st subdirectory - easier to see and for **Jupyter**
- it must be usable from **Jupyter** as subrirectory *very unusual, but useful*
    - for this, `jupyter notebook` is run in the above directory
	    - tests are made in notebook - like at `86:8888/tree/work/z_tests`
		- `import nuphy2`
		- `import nuphy2.rolfs`
		- `import nuphy2.version as v` and `print(v.__version__)`
		- `import nuphy2.isotope` - here is the problem, this module refers `data/`


### Problem of Jupyter and data subfolders
The problem was solvable wint `__init__.py` and subdirectory `nuphy2`.
```
import sys
sys.path.append(".")   # can find version in . by pytest
sys.path.append("./nuphy2")   # can find version from Jupyter
#@sys.path.append("./nuphy2/data")   # ????
#@sys.path.append("./data")   # ????
from version import __version__
try:
    from nuphy2.version import __version__
except:
    print()
```
